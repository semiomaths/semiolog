from setuptools import setup

setup(name='semiolog',
      version='0.1',
      description='Semiological analysis',
      url='https://gitlab.ethz.ch/semiomaths/semiolog',
      author='Juan Luis Gastaldi',
      author_email='juan.luis.gastaldi@gess.ethz.ch',
      license='CC BY-NC 4.0',
      packages=['semiolog'],
      zip_safe=False)